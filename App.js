import React from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button
          onClick={() => {
            if (window.cordova) {
              if (window.cordova.plugins) {
                alert("Found");
                window.cordova.plugins.notification.local.schedule({
                  title: "My first notification",
                  text: "Thats pretty easy...",
                  foreground: true,
                });
              } else alert("plugin not found");
            } else alert("Not found");
          }}
        >
          Get Notified
        </button>
      </header>
    </div>
  );
}

export default App;
